const stringproblem1 = (string) => {
    if (string != undefined ) {
        const digits = string.replace("$", "").replace(",", "")
        if (isNaN(Number(digits) || string.length==0)) {
            return 0
        }
        else {
            return digits
        }
    }
    else {
        return 0
    }
}

module.exports = stringproblem1