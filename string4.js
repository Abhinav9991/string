const string4=(titleCase)=>{
    let fullName=""
    if(titleCase.first_name!=undefined){
        const titleCaseFirstName=titleCase.first_name.toLowerCase()
        fullName+=titleCaseFirstName[0].toUpperCase()+titleCaseFirstName.substring(1)+" "
    }
    if(titleCase.middle_name!=undefined){
        const titleCaseMiddlename=titleCase.middle_name.toLowerCase()
        fullName+=titleCaseMiddlename[0].toUpperCase()+titleCaseMiddlename.substring(1) +" "
    }
    if(titleCase.last_name!=undefined){
        const titleCaseLastName=titleCase.last_name.toLowerCase()
        fullName+=titleCaseLastName[0].toUpperCase()+titleCaseLastName.substring(1)
    }
    else{
        return []
    }
    
   
    return fullName
}

module.exports=string4