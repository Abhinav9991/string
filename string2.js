const string2 = (string) => {
    if (string != undefined) {
        const ip = string.split(".").map(component => {
            if (Number.isInteger(Number(component))) {
                return Number(component)
            }
            else {
                return null
            }
        })
        if (ip.includes(null)) {
            return []
        }
        else {
            return ip
        }
    }
    else {
        return []
    }
}
module.exports = string2