const string3=(date)=>{
    let allMonths= ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    if(date.split("/").length===3){

        const month= Number(date.split("/")[1])
        const day=Number(date.split("/")[0])
        const year=(date.split("/")[2])
        if( month>0 && month<=12 && day>0 && day<=31 && !isNaN(year)){
            return allMonths[month-1]
        }
        else{
            return []
        }
    }
    else{
        return []
    }


}
module.exports=string3